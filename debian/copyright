Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Flask-Gravatar
Upstream-Contact: Alexander Zelenyak <zzz.sochi@gmail.com>
Source: https://github.com/zzzsochi/Flask-Gravatar

Files: *
Copyright:
  2010, Armin Ronacher
  2011, Max Countryman
  2011-2015, Alexander Zelenyak
  2012, Honza Javorek
  2012, Klinkin
  2013, Donald Stufft
  2013, Julien Kauffmann
  2013, Li Chuangbo
  2013, Miguel Grinberg
  2014, Andrew Grigorev
  2014, Nauman Ahmad
  2014, Paul Bonser
  2014, Tom Powell
  2015, CERN
License: BSD-3-clause

Files: debian/*
Copyright: 2017, Adrian Vondendriesch
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.
